<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="{{ asset('css/edit.css') }}">
</head>
<body>

	<div class="header">
		<h1>Edit Product</h1>
	</div>
	<br>

	<div class="alerts">

		@if($errors->any())
		<div class="alert alert-danger">
			 <ul>
		            @foreach($errors->all() as $error)
		            <li>{{$error}}</li>
		            @endforeach
		     </ul>
	 	</div>
	     @endif

	</div>

	<div>
		<form action="{{url('product/'.$product->id)}}" method="POST">
			@csrf
	
			<div class="column">
			<label for="productName">Product Name:</label><br>
			<input type="text" id="productName" name="productName" value="{{old('productName') ?? $product->productName }}"><br>

			<label for="productDescription">Description:</label><br>
			<input type="text" id="productDescription" name="productDescription" value="{{old('productDescription') ?? $product->productDescription }}"><br>

			<label for="productType">Type:</label><br>
			<input type="text" id="productType" name="productType" value="{{old('productType') ?? $product->productType }}"><br>

			<div class="inputInt">
			<label for="stock">Stock:</label><br>
			<input type="number" id="stock" name="stock" value="{{old('stock') ?? $product->stock }}"><br>
			</div>

			<div class="inputInt">
			<label for="price">Price:</label><br>
			<input type="number" id="price" name="price" value="{{old('price') ?? $product->price }}"><br>
			</div>

			<button class="edit-button">Save Edit</button>
			<a href="/" class="edit-button cancel">Cancel</a>
			</div>


			{{ method_field('PUT') }}
		</form> 

			

	</div>
</body>
</html>
