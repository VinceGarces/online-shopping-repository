<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">	
</head>
<body>

	<div class="header">
		<h1>Online Shopping Inventory</h1>
	</div>
	<br>

	<div class="alerts">
		@if(count($errors)>0)
		<div class="alert alert-danger">
		 <ul>
	            @foreach($errors->all() as $error)
	            <li>{{$error}}</li>
	            @endforeach
	     </ul>
	    </div>
	     @endif

	    @if(session('success'))
			<div class="alert alert-success">{{session('success')}}</div>		
		@endif
	</div>

	<div>
		<form action="product" method="POST">
			@csrf

			<div class="column">
			<label for="productName">Product Name:</label><br>
			<input type="text" id="productName" name="productName"><br>
			<label for="productDescription">Description:</label><br>
			<input type="text" id="productDescription" name="productDescription"><br>
			<label for="productType">Type:</label><br>
			<input type="text" id="productType" name="productType"><br>

			<div class="inputInt">
			<label for="stock">Stock:</label><br>
			<input type="number" id="stock" name="stock" value=0><br>
			</div>
			<div class="inputInt">
			<label for="price">Price:</label><br>
			<input type="number" id="price" name="price" value=0><br>
			</div>
			<button type="submit">Add Product</button>
			</div>
			
		</form> 

	</div>
	<div class="column table">
		<table>
			<tr>
				<th>Product Name</th>
				<th>Description</th>
				<th>Type</th>
				<th>Stock</th>
				<th>Price</th>
				<th>Action</th>
			</tr>
			@php
			$x=0
			@endphp
		@foreach($data as $i)
			@php
			$x+=1
			@endphp
			<tr>
				<td>{{$x}}. {{$i->productName}}</td>
				<td>{{$i->productDescription}}</td>
				<td>{{$i->productType}}</td>
				<td>{{$i->stock}}</td>
				<td>{{$i->price}}</td>
				<td class="options">
					<div class="option-button">
						<form method="GET" action="/edit/{{$i->id}}">
					        {{ csrf_field() }}
				            <input type="submit" class="btn btn-warning" value="Edit">
						</form>
					</div>
					<div class="option-button">
						<form method="POST" action="product/{{$i->id}}">
					        {{ csrf_field() }}
					        {{ method_field('DELETE') }}
				            <input type="submit" class="btn btn-danger" value="Delete">
						</form>
					</div>

				</td>
			</tr>	
		@endforeach

		</table>
	</div>
</body>
</html>
