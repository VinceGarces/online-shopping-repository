<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function index()
    {
        $data = Product::all();
        return $data;
    }

    public function show($id)
    {
        $data = Product::find($id);
        return $data;
    }


    public function store(Request $request)
    {
        $request -> validate([
           'stock' => 'required',
           'price' => 'required',
           'productName' => 'required',
           'productType' => 'required',
           'productDescription' => 'required'
        ]);
       $product = new Product;
       $product -> productName = $request -> input('productName');
       $product -> productType = $request -> input('productType');
       $product -> productDescription = $request -> input('productDescription');
       $product -> stock = $request -> input('stock');
       $product -> price = $request -> input('price');
       $product -> save();

       return redirect('/')->with('success','Product Added!');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
      $request -> validate([
           'stock' => 'required',
           'price' => 'required',
           'productName' => 'required',
           'productType' => 'required',
           'productDescription' => 'required'
        ]);
        $product = Product::find($id);
        $product->update($request->all());
        return redirect('/')->with('success','Product Updated!');
    }

    public function destroy($id)
    {
        $data = Product::find($id);
        $data->delete();

        return redirect('/')->with('success','Product Deleted!');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('EditProduct',compact('product'));
    }

    public function dashboard()
    {
        $data = Product::all();
        return view('DashboardView',['data' => $data]);
    }

}
