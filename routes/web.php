<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;




Route::get('/',[ProductController::class,'dashboard']);
Route::get('edit/{productID}',[ProductController::class,'edit']);

Route::post('product',[ProductController::class,'store']);
Route::delete('product/{productID}',[ProductController::class,'destroy']);
Route::put('product/{productID}',[ProductController::class,'update']);
